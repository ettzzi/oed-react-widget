import React, { Component } from 'react';
import Balance from './components/Balance/Balance';
import Widget from './components/Widget/Widget';
import WidgetPanel from './components/Widget/WidgetPanel';
import SelectField from './components/SelectField/SelectField';
import InputField from './components/InputField/InputField';
import InputGroup from './components/InputGroup/InputGroup';
import Nav from './components/Nav/Nav';
import Button from './components/Button/Button';
import Exchange from './components/Exchange/Exchange';

import CurrencyExchange from './utils/currency-exchange';
import Endpoint from './utils/endpoint-service';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      from: 'GBP',
      to: 'EUR',
      fromValue: 0,
      toValue: 0,
      balance: {
        EUR: 50,
        GBP: 100,
        USD: 25
      },
      fetching: false
    };
    this.performTransfer = this.performTransfer.bind(this);
    this.onCurrencyAmountChange = this.onCurrencyAmountChange.bind(this);
  }

  componentDidMount() {
    this.fetchAndSetRates();
    this.props.subscribe(this.fetchAndSetRates);
  }

  fetchAndSetRates = async () => {
    const rates = await Endpoint.latest(['GBP', 'EUR', 'USD']);
    CurrencyExchange.set(rates, 'USD');
    this.setState({
      rates: { ...rates }
    });
  };

  performTransfer() {
    const { from, to, fromValue, toValue, balance } = this.state;
    const newBalance = { ...balance };

    const fromBalance = parseFloat(balance[from]) - parseFloat(fromValue);
    const toBalance = parseFloat(balance[to]) + parseFloat(toValue);

    newBalance[from] = fromBalance;
    newBalance[to] = toBalance;

    this.setState({
      fromValue: 0,
      toValue: 0,
      balance: newBalance
    });
  }

  onCurrencyChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onCurrencyAmountChange(amount) {
    let newAmount = amount > 0 ? amount : 0;
    if (amount > this.state.balance[this.state.from]) {
      newAmount = this.state.balance[this.state.from];
    }
    const convertedAmount = CurrencyExchange.convert(
      newAmount,
      this.state.from,
      this.state.to
    );

    this.setState({
      fromValue: newAmount,
      toValue: convertedAmount
    });
  }

  render() {
    const { from, fromValue, to, toValue, balance, rates } = this.state;
    return (
      <Widget>
        <Nav>
            {rates && <Exchange>{`1 ${from} = ${CurrencyExchange.convert(1, from, to).toFixed(4)} ${to}`}</Exchange>}
            <Button onClick={this.performTransfer}>Exchange</Button>
        </Nav>
        <WidgetPanel primary>
          <InputGroup id="from">
            <SelectField
              id="from"
              name="from"
              options={this.props.currencies.filter(
                currency => currency !== this.state.to
              )}
              selected={from}
              onSelectChange={e => this.onCurrencyChange(e)}
            />
          </InputGroup>
          <InputGroup id="fromValue">
            <InputField
              id="fromValue"
              min={0}
              max={balance[from]}
              name="fromValue"
              value={fromValue}
              onChange={e => this.onCurrencyAmountChange(e)}
            />
          </InputGroup>
          <Balance currency={from} balance={balance[from]} />
        </WidgetPanel>

        <WidgetPanel>
          <InputGroup id="to">
            <SelectField
              id="to"
              name="to"
              options={this.props.currencies.filter(
                currency => currency !== this.state.from
              )}
              selected={this.state.to}
              onSelectChange={e => this.onCurrencyChange(e)}
            />
          </InputGroup>
          <InputGroup id="toValue">
            <InputField
              id="toValue"
              name="toValue"
              value={toValue}
              disabled={true}
            />
          </InputGroup>
          <Balance currency={to} balance={balance[to]} />
        </WidgetPanel>
      </Widget>
    );
  }
}

export default App;
