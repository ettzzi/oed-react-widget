import React from 'react';

const Balance = (props) => (
    <div>{`Balance: ${props.balance.toFixed(2)} ${props.currency}`}</div>
);

export default Balance;
