import styled from 'styled-components';

const Button = styled.button`
    border: 0;
    background: none;
    font-size: 1rem;
    letter-spacing: normal;
    padding: 0.5 1rem;
    cursor: pointer;
    background:white;
`;


export default Button;
