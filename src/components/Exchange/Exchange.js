import React from 'react';
import styled from 'styled-components';

export default styled.p`
    padding: 1rem;
    color: #FFF;
`;
