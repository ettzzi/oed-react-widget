import React from 'react';
import styled from 'styled-components';
import NumericInput from 'react-numeric-input';

const InputField = styled(NumericInput)
    .attrs({style:false, precision: 2})`
    width: 100%;
    display: flex;
    padding: 0.5rem 1.875rem 0.5rem 0.5rem;
    appearance: none;
    border: 1px solid #a7b0b7;
    font-size: 1rem;
    color: #4a4a4a;
    font-weight: 300;
    letter-spacing: 1px;
    border-radius: 2px;
    box-shadow: none;
    line-height: 1.25rem;
    box-sizing: border-box;
    margin-bottom: 1rem;
`;

export default (props) => <InputField {...props}/>;

