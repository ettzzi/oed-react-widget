import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledInputGroup = styled.div`
  display: inline-block;
  margin: 0 0.25rem;
  color: #4a4a4a;
  font-weight: 300;
`;

const Label = styled.label`
  display: block;
`;
const InputGroup = ({ id, label, children }) => (
    <StyledInputGroup>
        <Label htmlFor={id}>{label}</Label>
        {children}
    </StyledInputGroup>
);

InputGroup.PropTypes = {
    label: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    children: PropTypes.node
};

export default InputGroup;
