import React from 'react';
import styled from 'styled-components';

const Nav = styled.nav`
    display: flex;
    background-color: rgb(44,113,243);
    justify-content: space-between;
    align-items: center;
    padding: 0 1rem;
`;

export default Nav;
