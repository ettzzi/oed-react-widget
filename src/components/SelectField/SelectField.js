import React from 'react';
import PropTypes from 'prop-types';

import Select from './Select';

const SelectField = ({ id, name, selected, options, onSelectChange }) => (
    <Select id={id} name={name} onChange={onSelectChange} defaultValue={selected}>
        {options.map((item) => (
            <option key={item} value={item}>
                {item}
            </option>
        ))}
    </Select>
);

SelectField.propTypes = {
    id: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(PropTypes.string).isRequired,
    onSelectChange: PropTypes.func.isRequired
};

export default SelectField;
