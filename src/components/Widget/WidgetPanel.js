import styled from 'styled-components';

const WidgetPanel = styled.div`
  background-color: ${props => props.primary ? 'rgb(44,113,243)' : 'rgb(46,95,202)'};
  color: #FFF;
  padding: 1rem;
`;

export default WidgetPanel;
