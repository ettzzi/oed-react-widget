import React, {Component} from 'react';

export default function withInterval(WrappedComponent, intervalLength) {
    return class extends Component {
        constructor(props) {
            super(props);
            this.listeners = [];

            this.subscribe = this.subscribe.bind(this);
            this.dispatch = this.dispatch.bind(this);
        }

        componentWillMount() {
            this.timerId = setInterval(this.dispatch, intervalLength);
        }

        componentWillUnmount() {
            clearInterval(this.timerId);
        }

        subscribe(callback) {
            this.listeners.push(callback);
        }

        dispatch() {
            this.listeners.forEach(func => func());
        }

        render() {
            return <WrappedComponent subscribe={this.subscribe} {...this.props} />;
        }
    };
}
